#include <iostream>
#include <string>

using namespace std;

enum Type {
	NOTHING,
	PLUS,
	MINUS,
	MULTIPLY,
	DIVID,
	LPARENTHESES,
	RPARENTHESES,
	NUMBER
};

struct Token {
    Type type = NOTHING;
    int value = 0;
};

Type findType(char);

void printAllData(Token *,int);

int main(){
	string userInput;
	string number = "";
	int flag = 0;
	int i=0;
	cin >> userInput;
	int stringLength = userInput.length();
	Token tokenData[stringLength];
	int numberOfTokenData = 0;
	while (userInput[i] != 0){
		if(userInput[i] <= '9' && userInput[i] >= '0'){
			number += userInput[i];
			flag = 1;
		}
		else{
			if(flag==1){
				tokenData[numberOfTokenData].type = NUMBER;
				tokenData[numberOfTokenData].value = stoi(number);
				number = "";
				numberOfTokenData += 1;
			}
			tokenData[numberOfTokenData].type = findType(userInput[i]);
			numberOfTokenData += 1;
			flag = 0;
		}
		i++;
	}
	if (flag == 1){
		tokenData[numberOfTokenData].type = NUMBER;
		tokenData[numberOfTokenData].value = stoi(number);
		numberOfTokenData += 1;
	}
	printAllData(tokenData,numberOfTokenData);
	return 0;
}

Type findType(char character){
	switch(character) {
		case '+':
			return PLUS;
		case '-':
			return MINUS;
		case '*':
			return MULTIPLY;
		case '/':
			return DIVID;
		case '(':
			return LPARENTHESES;
		case ')':
			return RPARENTHESES;
	}
	return NOTHING;
}

void printAllData(Token *tokenData,int numberOfTokenData){
	for(int i=0;i<numberOfTokenData;i++){
		switch(tokenData[i].type){
			case NOTHING :
				cout << "Nothing" << endl;
				break;
			case PLUS :
				cout << "PLUS" << endl;
				break;
			case MINUS :
				cout << "MINUS" << endl;
				break;
			case MULTIPLY :
				cout << "MULTIPLY" << endl;
				break;
			case DIVID :
				cout << "DIVID" << endl;
				break;
			case LPARENTHESES :
				cout << "LPARENTHESES" << endl;
				break;
			case RPARENTHESES :
				cout << "RPARENTHESES" << endl;
				break;
			case NUMBER :
				cout << tokenData[i].value << endl;
				break;
		}
	}
}
