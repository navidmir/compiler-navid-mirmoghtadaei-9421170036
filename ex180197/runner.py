class assemblyRunner :
	def __init__(self) :
		self.assembly = []
		self.fileName = "assembly.txt"
		self.registers = [None for i in range(1000)]
	
	def readFile(self) :
		self.assembly = open(self.fileName,'r').read().splitlines()
	def main(self) :
		for command in self.assembly :
			operatorLength = command.index(" ")
			operator = command[:operatorLength]
			arguments = command[operatorLength+1:]

			if operator == "MOV" :
				self.mov(arguments)
			
			elif operator == "ADD" :
				self.add(arguments)
			
			elif operator == "SUB" :
				self.sub(arguments)

			elif operator == "DIV" :
				self.div(arguments)

			elif operator == "MUL" :
				self.mul(arguments)

		print("the answer is : ")
		print(self.returnAnswer())

	def mov(self,arguments) :
		arguments = arguments.split(",")
		arg1 = int(arguments[0][1:])
		arg2 = float(arguments[1])
		self.registers[arg1] = arg2

	def add(self,arguments) :
		arguments = arguments.split(",")
		arg1 = int(arguments[0][1:])
		arg2 = int(arguments[1][1:])
		self.registers[arg1] += self.registers[arg2]

	def sub(self,arguments) :
		arguments = arguments.split(",")
		arg1 = int(arguments[0][1:])
		arg2 = int(arguments[1][1:])
		self.registers[arg1] -= self.registers[arg2]

	def div(self,arguments) :
		arguments = arguments.split(",")
		arg1 = int(arguments[0][1:])
		arg2 = int(arguments[1][1:])
		self.registers[arg1] /= self.registers[arg2]

	def mul(self,arguments) :
		arguments = arguments.split(",")
		arg1 = int(arguments[0][1:])
		arg2 = int(arguments[1][1:])
		self.registers[arg1] *= self.registers[arg2]

	def returnAnswer(self) :
		lastCommand = self.assembly[len(self.assembly)-1]
		answerRegister =  int(((lastCommand.split(" ")[1]).split(",")[0])[1:])
		return self.registers[answerRegister]

if __name__ == "__main__" :
	runner = assemblyRunner()
	runner.readFile()
	runner.main()